from flask_restful import Resource, Api
from flask import Flask, request
from flask_cors import CORS
from tensorflow.keras.models import load_model
import os
import numpy as np
import cv2

app = Flask(__name__)
api= Api(app)
cors = CORS(app)
model = load_model('model.h5')

#UPLOAD_FOLDER = "/images"
#app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
class Upload(Resource):

    def post(self):
        file = request.files['image']
        file.save("predict.JPG")


        return {"status": "ok"}

class Predict(Resource):

    def get(self):
        num_features = 64
        num_labels = 7
        batch_size = 64
        epochs = 1
        width, height = 48, 48
        emotion_dict = {0: "Angry", 1: "Disgust", 2: "Fear", 3: "Happy", 4: "Sad", 5: "Surprise", 6: "Neutral"}
        frame = cv2.imread('predict.JPG')
        expression = ""
        # ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 1)
            roi_gray = gray[y:y + h, x:x + w]
            cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray, (48, 48)), -1), 0)
            cv2.normalize(cropped_img, cropped_img, alpha=0, beta=1, norm_type=cv2.NORM_L2,
            dtype=cv2.CV_32F)
            prediction = model.predict(cropped_img)
            expression = emotion_dict[int(np.argmax(prediction))]
            cv2.putText(frame, emotion_dict[int(np.argmax(prediction))], (x, y),
            cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 1, cv2.LINE_AA)
        """
        Pass the expression to music player from here
        """
        print(expression)
        return expression 

api.add_resource(Upload, '/upload')
api.add_resource(Predict, '/predict')

if __name__ == '__main__':
    app.run()